class colors {
  colors({this.name});
  var name;

  @override
  String get getMe => "Color is $name";
  void getName() => print("Color is $name");
  String toString() => "name: $name";
}

class mixed extends colors {
  mixed({var name, this.mix}) : super(name: name);
  var mix;

  @override
  String toString() => '${super.toString()}, mix: $mix';

}


void main() {
  final mix = mixed(name: 'Green', mix: 'Yellow + Blue');
  mix.getName();
  print(mix.toString());
  
  final mix1 = mixed(name: 'Orange', mix: 'Yellow + Red');
  mix1.getName();
  print(mix1.toString());

  final mix2 = mixed(name: 'Pink', mix: 'White + Red');
  mix2.getName();
  print(mix2.toString());

  final mix3 = mixed(name: 'Purple', mix: 'Blue + Red');
  mix3.getName();
  print(mix3.toString());

  final mix4 = mixed(name: 'Violet', mix: 'Purple + Red');
  mix3.getName();
  print(mix4.toString());
 
  
}
